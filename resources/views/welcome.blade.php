<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
</head>
<body>
    <div id="app">
        <!-- Header -->
        <div class="navbar" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <div class="logo">
                        <a class="navbar-brand" href="/">
                        <span class="logo__image img-responsive">
                            <img src="{{ asset('images/honey-hunters.png') }}" alt="honey-hunters" class="img-responsive">
                        </span>
                            <span class="logo__name">{{ env('APP_NAME') }}</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!--/ Header -->

        <!-- Comments -->
        <comments></comments>
        <!--/ Comments -->

        <!-- Footer -->
        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="logo">
                            <a class="" href="/">
                            <span class="logo__image img-responsive">
                                <img src="{{ asset('images/honey-hunters.png') }}" alt="honey-hunters" class="img-responsive">
                            </span>
                                <span class="logo__name">{{ env('APP_NAME') }}</span>
                            </a>
                        </div>
                        <div class="social pull-right">
                            <a href="">
                            <span class="social__vk">
                                 <i class="fa fa-vk" aria-hidden="true"></i>
                            </span>
                            </a>
                            <a href="">
                            <span class="social__fb">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/ Footer -->
    </div>

    <script src="{{ asset('/js/app.js') }}"></script>
</body>
</html>